#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import commands

"""Copy Special exercise
"""


# +++your code here+++
# Write functions and modify main() to call them
def get_special_paths(dir):
  result = []
  for f in os.listdir(dir):
    result.append(os.path.abspath(f))

  return result


def copy_to(paths, dir):
    for path in paths:
        shutil.copy(path, dir)


def main():
    # This basic command line argument parsing code is provided.
    # Add code to call your functions below.

    # Make a list of command line arguments, omitting the [0] element
    # which is the script itself.
    args = sys.argv[1:]
    if not args:
        print "usage: [--todir dir][--tozip zipfile] dir [dir ...]";
        sys.exit(1)

    # todir and tozip are either set from command line
    # or left as the empty string.
    # The args array is left just containing the dirs.
    to_dir = ''
    if args[0] == '--todir':
        to_dir = args[1]
        dir_name = args[2]
    paths = get_special_paths(dir_name)
    copy_to(paths,to_dir)
    del args[0:2]

    to_zip = ''
    if args[0] == '--tozip':
        to_zip = args[1]
        del args[0:2]

    if len(args) == 0:
        print "error: must specify one or more dirs"
        sys.exit(1)

    # +++your code here+++
    # Call your functions


if __name__ == "__main__":
    main()
